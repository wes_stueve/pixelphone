﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Microsoft.Owin.Hosting;

namespace PixelPhone
{
	public static class SignalRHelper
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(SignalRHelper));
		public static IDisposable SignalR { get; set; }
		const string ServerURI = "http://localhost:8080";

		public static void StartServer()
		{
			try
			{
				Log.Debug("Starting Server");
				SignalR = WebApp.Start(ServerURI);
			}
			catch (TargetInvocationException tie)
			{
				Log.Error("TIE starting the server.", tie);
				return;
			}
			catch (Exception e)
			{
				Log.Error("Exception starting signalR", e);
			}
			Log.Debug("Server started at " + ServerURI);
		}
	}
}
