﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using AForge.Imaging.Filters;
using dshow;
using dshow.Core;
using Microsoft.AspNet.SignalR;
using PixelPhone.Controls;
using PixelPhone.VideoSource;
using PlayerDetection;
using VideoSource;

namespace PixelPhone
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly FilterCollection _filterCollection;
        private FiltersSequence _filtersSequence;
        private PlayerDetectionator _playerDetection;
        private IVideoSource _videoSource;
        private ICamera _camera;
        private IList<Color> _detectionColors = new List<Color> { Color.FromArgb(255, 0, 106), Color.FromArgb(0, 1, 255), Color.FromArgb(255, 247, 0) };
        

        public MainWindow()
        {
            InitializeComponent();
            _filterCollection = new FilterCollection(FilterCategory.VideoInputDevice);

                        
            PlayerStates.PlayerConnected += PlayerConnect;
            PlayerStates.PlayerDisconnected += PlayerStatesOnPlayerDisconnected;

            foreach (Filter filter in _filterCollection)
            {
                VideoSourceCb.Items.Add(filter.Name);
            }
            VideoSourceCb.SelectedIndex = 0;
            VideoCanvas.FrameRendered += ProcessOutput;
            StartVideo();
			StartupHelper.Startup();
            
        }

        

        private string FindVideoSourceMoniker()
        {
            string selectedValue = (VideoSourceCb.SelectedValue ?? string.Empty).ToString();
            return
                (from Filter filter in _filterCollection where filter.Name == selectedValue select filter.MonikerString)
                    .FirstOrDefault();
        }

        private void StartVideo()
        {
            string vidSourceMoniker = FindVideoSourceMoniker();
            _videoSource = new CaptureDevice(vidSourceMoniker);
            _camera = new Camera(_videoSource);
            VideoCanvas.Camera = _camera;
            
            _playerDetection = new PlayerDetectionator();

            for (var i = 0; i < Math.Min(3, Math.Min(PlayerStates.ConnectedPlayers.Count - 1, 3)); i++)
            {
                var player = PlayerStates.ConnectedPlayers[i];
                _playerDetection.Add(new PlayerDetector(_detectionColors[i], player));
            }

            VideoCanvas.PlayerDetectionator = _playerDetection;
            _videoSource.Start();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            StopCamera();
            base.OnClosing(e);
        }

        private void VideoSourceCb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //StopCamera();
            //StartVideo();
        }


        private void StopCamera()
        {
            if (_camera != null && _camera.Running)
            {
                // signal camera to stop
                _camera.SignalToStop();
                // wait for the camera
                _camera.Stop();
            }
        }

        private void PlayerConnect(object sender, EventArgs e)
        {
            if (_playerDetection.Any(x => x.Player.ConnectionId == sender.ToString())
                && _playerDetection.Count >= 3)
                return;
            var player = PlayerStates.ConnectedPlayers.FirstOrDefault(x => x.ConnectionId == sender.ToString());
            if (player == null) return;
            var index = _playerDetection.Count > 0 ? _playerDetection.Count - 1 : 0;
            var color = _detectionColors[index];
            _playerDetection.Add(new PlayerDetector(color, player));
        }

        private void PlayerStatesOnPlayerDisconnected(object sender, EventArgs eventArgs)
        {
            var playerDetection = _playerDetection.FirstOrDefault(x => x.Player.ConnectionId.Equals(sender.ToString()));
            if (playerDetection == null) return;
            //ayerDetection.Remove(playerDetection);
        }

        private void ProcessOutput(object sender, EventArgs e)
        {
            var playerDeet = sender as IPlayerDetail;
            Task.Factory.StartNew(() => ProcessOutputAsync(playerDeet));
        }

        private void ProcessOutputAsync(IPlayerDetail playerDeet)
        {
            
            if (playerDeet == null) return;


            if (playerDeet.Player.IsDrawing)
            {
                PhoneHubInstance.DrawLine(playerDeet.Player.CurrentColor,
                    5,
                    (int)playerDeet.Location.X,
                    (int)playerDeet.Location.Y,
                    (int)playerDeet.PreviousLocation.X,
                    (int)playerDeet.PreviousLocation.Y,
                    playerDeet.ExtentX,
                    playerDeet.ExtentY,
                    playerDeet.Player.ConnectionId);
            }
            else
            {
                PhoneHubInstance.SetPointer(
                    (int)playerDeet.Location.X,
                    (int)playerDeet.Location.Y,
                    playerDeet.ExtentX,
                    playerDeet.ExtentY,
                    playerDeet.Player.ConnectionId);
            }
        }
    }
}
