﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using log4net;
using Microsoft.AspNet.SignalR;

namespace PixelPhone
{
	public class PhoneHub : Hub
	{

	    public IList<ConnectedPlayer> ConnectedPlayers
	    {
	        get
	        {
	            return PlayerStates.ConnectedPlayers; 
	        }
	    }
		private readonly ILog log = LogManager.GetLogger(typeof(PhoneHub));
		Random r = new Random();


		public void HidePointer(string connectionId)
		{
			Clients.All.hidePointer(connectionId);
		}

		public void ClearCanvas()
		{
			Clients.All.clearCanvas();
		}

		public void SetPointer(int x1, int y1, int e1, int e2, string connectionId)
		{
			Clients.All.setPointer(x1, y1, e1, e2, connectionId);
		}

		public void DrawLine(string color, int strokeWidth, int x1, int y1, int x2, int y2, int e1, int e2, string connectionId)
		{
			Clients.All.drawLine(color, strokeWidth, x1, y1, x2, y2, e1, e2, connectionId);
		}

		public void Send(string name, string message)
		{
			DrawLine("#000000", r.Next(10), r.Next(1080), r.Next(720), r.Next(1080), r.Next(720), 1080, 720, r.Next(3).ToString());
			Clients.All.addMessage(name, message);
		}

        public void Interpret(string message)
        {
            var player = ConnectedPlayers.FirstOrDefault(x => x.ConnectionId.Equals(Context.ConnectionId));
            if (player == null)
            {
                HidePointer(Context.ConnectionId);
                return;
            }

	        var nterp = new VoiceInterpreter();
	        var color = nterp.Interpret(message);

            if (!string.IsNullOrWhiteSpace(color.Url))
                Clients.All.changeBackGround(color.Url);

            if (color.IsClear)
            {
                ClearCanvas();
            }

            if (!color.FoundColor) return; 
            
            var mediacolor = color.PalletColor;
            var c = ColorTranslator.ToHtml(Color.FromArgb(mediacolor.A, mediacolor.R, mediacolor.G, mediacolor.B));
            
            Clients.Caller.getColor(c);

            player.CurrentColor = c;
        }

	    public void Register()
	    {
            PlayerStates.OnPlayerConnected(Context.ConnectionId);
	    }
		public override Task OnConnected()
		{
			//Use Application.Current.Dispatcher to access UI thread from outside the MainWindow class
			Application.Current.Dispatcher.Invoke(() => log.Debug("Client connected: " + Context.ConnectionId));
			return base.OnConnected();
		}
		public override Task OnDisconnected(bool stopCalled)
		{
			//Use Application.Current.Dispatcher to access UI thread from outside the MainWindow class
			Application.Current.Dispatcher.Invoke(() => log.Debug("Client disconnected: " + Context.ConnectionId));
            PlayerStates.OnPlayerDisconnected(Context.ConnectionId);
			return base.OnDisconnected(stopCalled);
		}

	    public void ToggleDraw(bool isDrawing)
	    {
            var player = ConnectedPlayers.FirstOrDefault(x => x.ConnectionId.Equals(Context.ConnectionId));
            if (player == null)
            {
                HidePointer(Context.ConnectionId);
                return;
            }

	        player.IsDrawing = isDrawing;
	    }
	}


}
