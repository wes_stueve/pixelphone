﻿using System.Linq;

using System.Windows.Media;

namespace PixelPhone
{
    public class VoiceInterpreter : IInterpreter
    {
        public Interpreted Interpret(string message)
        {

            var words = message.Split(' ');
            foreach (var word in words.Reverse())
            {
                if (word.ToUpper() == "CLEAR")
                {
                    return new Interpreted
                    {
                        IsClear = true
                    };
                }

                if (word.ToUpper() == "POOH")
                {
                    return new Interpreted
                    {
                        Url = "winniethepooh.jpg"
                    };
                }
                if (word.ToUpper() == "1")
                {
                    return new Interpreted
                    {
                        Url = "elmo.jpg"
                    };
                }

                if (word.ToUpper() == "2")
                {
                    return new Interpreted
                    {
                        Url = "ninjaturtle.jpg"
                    };
                }
                if (word.ToLower() == "none")
                {
                    return new Interpreted
                    {
                        Url = "none.jpg"
                    };
                }

                if (word.ToUpper() == "BUTTERFLY" || word.ToLower() == "butterflies")
                {
                    return new Interpreted
                    {
                        Url = "butterflies.jpg"
                    };
                }

                var colorValue = 0;
                if (word.ToUpper() == "ERASE")
                {
                    colorValue = System.Drawing.Color.FromName("white").ToArgb();
                }


                if (colorValue == 0)
                    colorValue = System.Drawing.Color.FromName(word).ToArgb();
                
                
                var colorHex = string.Format("{0:x6}", colorValue);
                if (ColorConverter.ConvertFromString("#" + colorHex) is Color)
                {
                    var convertFromString = ColorConverter.ConvertFromString("#" + colorHex);
                    if (convertFromString != null)
                    {

                        return new Interpreted
                        {
                            FoundColor = true,
                            PalletColor = (Color)convertFromString

                        };

                    }
                }
            }


            return new Interpreted { FoundColor = false };
        }
    }

    public class Interpreted
    {
        public bool FoundColor { get; set; }
        public Color PalletColor { get; set; }
        public bool IsClear { get; set; }
       
        public string Url { get; set; }
    }
}
