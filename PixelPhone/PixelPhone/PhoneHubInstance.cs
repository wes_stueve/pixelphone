﻿using System;
using Microsoft.AspNet.SignalR;

namespace PixelPhone
{
    public class PhoneHubInstance
    {

        // Singleton instance
        private readonly static Lazy<IHubContext> _instance = new Lazy<IHubContext>(
            () => (GlobalHost.ConnectionManager.GetHubContext<PhoneHub>()));

        private static IHubContext _context;

        private PhoneHubInstance(IHubContext context)
        {
            _context = context;
        }



        // This method is invoked by a Timer object.
        //private void UpdateStockPrices(object state)
        //{
        //    foreach (var stock in _stocks.Values)
        //    {
        //        if (TryUpdateStockPrice(stock))
        //        {
        //            _context.Clients.All.updateStockPrice(stock);
        //        }
        //    }
        //} 

        public static void DrawLine(string color, int strokeWidth, int x1, int y1, int x2, int y2, int e1, int e2,
            string connectionId)
        {
            _instance.Value.Clients.All.drawLine(color, strokeWidth, x1, y1, x2, y2, e1, e2, connectionId);
        }

        public static void SetPointer(int x1, int y1, int e1, int e2, string connectionId)
        {
            _instance.Value.Clients.All.setPointer(x1, y1, e1, e2, connectionId);
        }
    }
}