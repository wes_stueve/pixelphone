﻿using System.Drawing;
using PlayerDetection;

namespace PixelPhone
{
    public class ConnectedPlayer : IPlayer
    {

        public string ConnectionId { get; private set; } 
        public string Name { get; set; }

        public string CurrentColor
        {
            get;
            set;
        }

        public bool IsDrawing
        {
            get;
            set;
        }
        public ConnectedPlayer(string connectiionId)
        {
            ConnectionId = connectiionId;
            CurrentColor = "#000000";
            Name = "Player";
        }

        public Coordinates PreviousLocation
        {
            get; set;
        }

        public Coordinates Location
        {
            get; set;
        }
    }
}