﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Xml;
using AForge;
using PixelPhone.VideoSource;
using PlayerDetection;
using VideoSource;
using Color = System.Drawing.Color;
using Pen = System.Drawing.Pen;

namespace PixelPhone.Controls
{
    public class VideoCanvasUc : MediaElement
    {
        private ICamera _videoSource;
        private Bitmap _background = null;
        private object _frameLock = new object();
        public event EventHandler FrameRendered;

        private Bitmap Background
        {
            get
            {
                lock (_frameLock)
                {
                    return _background;
                }
            }
            set
            {
                lock (_frameLock)
                {
                    if (_background != null)
                        _background.Dispose();
                    _background = value;
                }
            }
        }

        public ICamera Camera
        {
            get
            {
                return _videoSource;
            }
            set
            {
                Monitor.Enter(this);
                _videoSource = value;
                Camera.NewFrame += VideoSourceOnNewFrame;
                Monitor.Exit(this);
            } 
        }

        public PlayerDetectionator PlayerDetectionator { get; set; }

        private void VideoSourceOnNewFrame(object sender, EventArgs cameraEventArgs)
        {
            try
            {
                if (Monitor.IsEntered(this)) return;
                Background = ((ICamera)sender).LastFrame;
                Dispatcher.Invoke(ProcessFrame,
                    DispatcherPriority.Render);
            }
            catch
            {
            }
        }

        protected override void OnRender(DrawingContext dc)
        {

            try
            {
                Monitor.Enter(this);
                if (Background == null) return;
                using (var tempImage = (Bitmap) Background.Clone())
                {     
                    var imageSource = Imaging.CreateBitmapSourceFromHBitmap(tempImage.GetHbitmap(), IntPtr.Zero,
                        Int32Rect.Empty,
                        BitmapSizeOptions.FromEmptyOptions());
                    dc.DrawImage(imageSource, new Rect(0, 0, _background.Width, _background.Height));
                    
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                Monitor.Exit(this);
            }
        }

        private void ProcessFrame()
        {
            Monitor.Enter(this);
            var playerDeets = PlayerDetectionator.Detect(Background);
            DrawPlayerFrame(playerDeets);
            if (FrameRendered != null)
                System.Threading.Tasks.Parallel.ForEach(playerDeets, player => FrameRendered.Invoke(player, new EventArgs()));
            InvalidateVisual();
            Monitor.Exit(this);
        }

        private void DrawPlayerFrame(IList<IPlayerDetail> playerDeets)
        {
            if (playerDeets.Any())
            {
                // create graphics object from initial image
                Graphics g = Graphics.FromImage(Background);

                using (Pen pen = new Pen(Color.GreenYellow, 1))
                {
                    int n = 0;

                    // draw each rectangle
                    foreach (var player in playerDeets)
                    {
                        if (player != null)
                            g.DrawRectangle(pen, player.DetectedLocation);


                    }
                }
                g.Dispose();
            }
        }

        protected override void OnInitialized(EventArgs e)
        {
            
            base.OnInitialized(e);
        }


    }
}