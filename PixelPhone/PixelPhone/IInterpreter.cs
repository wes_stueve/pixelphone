﻿using System.Windows.Media;

namespace PixelPhone
{
    public interface IInterpreter
    {
        Interpreted Interpret(string message);
    }
}
