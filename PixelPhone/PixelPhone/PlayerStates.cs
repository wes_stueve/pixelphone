﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PixelPhone
{
    public static class PlayerStates
    {
        private static IList<ConnectedPlayer> _connectedPlayers; 
        public static IList<ConnectedPlayer> ConnectedPlayers
        {
            get
            {
                return _connectedPlayers ?? (_connectedPlayers = new List<ConnectedPlayer>());
            }
        }

        public static event EventHandler PlayerConnected;

        public static void OnPlayerConnected(string connectionId)
        {
            if (!ConnectedPlayers.Any(x => x.ConnectionId.Equals(connectionId)))
                ConnectedPlayers.Add(new ConnectedPlayer(connectionId));

            if (PlayerConnected != null)
                PlayerConnected.Invoke(connectionId, new EventArgs());

            var handler = PlayerConnected;
            if (handler != null) handler(connectionId, EventArgs.Empty);
        }

        public static event EventHandler PlayerDisconnected;

        public static void OnPlayerDisconnected(string connectionId)
        {
            var player = ConnectedPlayers.FirstOrDefault(x => x.ConnectionId.Equals(connectionId));
            if (player != null) ConnectedPlayers.Remove(player);
            EventHandler handler = PlayerDisconnected;
            if (handler != null) handler(connectionId, EventArgs.Empty);
        }
    }
}