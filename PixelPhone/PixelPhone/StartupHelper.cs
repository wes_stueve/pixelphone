﻿using System.Threading.Tasks;
using log4net.Config;

namespace PixelPhone
{
	public static class StartupHelper
	{
		public static void Startup()
		{
			XmlConfigurator.Configure();
			Task.Run(() => SignalRHelper.StartServer());
		}
	}
}
