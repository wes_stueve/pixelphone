﻿using System.Drawing;

namespace PixelPhone.Filters
{
    public interface IFrameFilter
    {
        void ProcessFrame(Bitmap frame);
    }
}