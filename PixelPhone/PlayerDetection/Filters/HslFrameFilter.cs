﻿using System;
using System.Drawing;
using AForge;
using AForge.Imaging.Filters;

namespace PixelPhone.Filters
{
    public class HslFrameFilter : IFrameFilter
    {
        private readonly HSLFiltering _filter;
        public HslFrameFilter(Color color, float threshold)
        {
            var hue = (int)color.GetHue();
            hue = (Math.Max(Math.Min(0, hue), hue));
            _filter = new HSLFiltering
            {
                Hue = new IntRange((int)(hue * threshold), (int)(hue / threshold)),
                Saturation = new Range(0.6f, 1.0f),
                Luminance = new Range(0.1f, 0.5f),
                UpdateLuminance = true,
                UpdateHue = true,
                //FillOutsideRange = true
            };
        }

        public void ProcessFrame(Bitmap image)
        {
            _filter.ApplyInPlace(image);
        }
    }
}