﻿using System;
using System.Drawing;

namespace PlayerDetection
{
    public interface ICamera
    {
        event EventHandler NewFrame;
        event EventHandler Alarm;
        Bitmap LastFrame { get; }
        int Width { get; }
        int Height { get; }
        int FramesReceived { get; }
        int BytesReceived { get; }
        bool Running { get; }
        void Start();
        void SignalToStop();
        void WaitForStop();
        void Stop();
        void Lock();
        void Unlock();
        bool StopMe { get; set; }
    }
}