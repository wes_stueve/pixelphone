﻿using System.Drawing;

namespace PlayerDetection
{
    public interface IPlayer
    {
        string ConnectionId { get; } 
        string Name { get; set; }
        string CurrentColor { get; }
        bool IsDrawing { get; }
        Coordinates PreviousLocation { get; set; }
        Coordinates Location
        {
            get;
            set;
        }
    }
}