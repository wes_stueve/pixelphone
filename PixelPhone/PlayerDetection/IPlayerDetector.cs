﻿using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using AForge.Imaging;
using PixelPhone.Filters;

namespace PlayerDetection
{
    public interface IPlayerDetector
    {
        Color DeviceColor { get; }
        Task<IPlayerDetail> DetectPlayer(Bitmap frame);
        IPlayer Player { get; }
    }

    public class PlayerDetector : IPlayerDetector
    {
        private readonly IFrameFilter _filter;
        
        public Color DeviceColor
        {
            get; private set;
        }

        public IPlayer Player { get; private set; }

        public PlayerDetector(Color deviceColor, IPlayer player)
        {
            DeviceColor = deviceColor;
            _filter = new HslFrameFilter(deviceColor, 0.85f);
            Player = player;
        }

        public async Task<IPlayerDetail> DetectPlayer(Bitmap frame)
        {
            _filter.ProcessFrame(frame);
            var rects = await GetCoordinates(frame);

            if (rects != null && rects.Any() && rects.Any(x=>(x.Width + x.Height) > 20))
            {

                var largestRect = rects.Aggregate((r1, r2) => (r1.Height*r1.Width) > (r2.Height*r2.Width) ? r1 : r2);

                return new PlayerDetail(largestRect, Player, frame.Width, frame.Height);
            }
            return new PlayerDetail(Player, frame.Width, frame.Height);
        }

        private async Task<Rectangle[]> GetCoordinates(Bitmap bitmap)
        {
            var blobcounter = new BlobCounter(bitmap);
            return blobcounter.GetObjectsRectangles();    
        }
    }
}