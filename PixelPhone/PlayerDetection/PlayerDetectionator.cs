﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using AForge.Imaging.Filters;

namespace PlayerDetection
{
    public class PlayerDetectionator : ConcurrentBag<IPlayerDetector>
    {
        private readonly Mirror _mirror;
        public PlayerDetectionator()
        {
            _mirror = new Mirror(false, true);
        }

        public IList<IPlayerDetail> Detect(Bitmap frame)
        {
            var playerDetails = new ConcurrentBag<IPlayerDetail>();
            _mirror.ApplyInPlace(frame);
            var tasks = new List<Task>();
            foreach (var playerDetector in this)
            {
                using (var bitmap = (Bitmap)frame.Clone())
                {
                    tasks.Add(DoDetection(playerDetector, bitmap, playerDetails));
                }
            }
            Task.WaitAll(tasks.ToArray());
            return playerDetails.ToList();

        }

        private async Task DoDetection(IPlayerDetector detector, Bitmap frame, ConcurrentBag<IPlayerDetail> playerDetails)
        {
            var playerDetail = await detector.DetectPlayer(frame);
            if (playerDetail != null)
                playerDetails.Add(playerDetail);
        }
    }
}
