﻿
using System.Drawing;

namespace PlayerDetection
{
    public interface IPlayerDetail
    {
        Rectangle DetectedLocation { get;  }
        Coordinates Location { get; set; }
        Coordinates PreviousLocation { get; }
        
        int ExtentX { get; set; }
        int ExtentY { get; set; }
        IPlayer Player { get; }

    }

    public class PlayerDetail : IPlayerDetail
    {

        public Coordinates Location
        {
            get { return Player.Location; }
            set
            {
                if (Player.PreviousLocation == null)
                    Player.PreviousLocation = value;
                if (!Player.PreviousLocation.Equals(Player.Location))
                    Player.PreviousLocation = Player.Location;
                Player.Location = value;
            }
        }

        public Coordinates PreviousLocation
        {
            get
            {
                if (Player == null || Player.PreviousLocation == null)
                    return null;
                return Player.PreviousLocation;
            }
        }
        public int ExtentX { get; set; }
        public int ExtentY { get; set; }


        public IPlayer Player { get; private set; }

        public PlayerDetail(Rectangle rect, IPlayer player, int extentX, int extentY)
        {
            Player = player;
            Location = new Coordinates(rect.X, rect.Y);
            
            DetectedLocation = rect;
            
            ExtentX = extentX;
            ExtentY = extentY;
        }

        public PlayerDetail(IPlayer player, int extentX, int extentY)
        {
            Player = player;
            ExtentX = extentX;
            ExtentY = extentY;
            Location = new Coordinates(0f, 0f);
        }

        public Rectangle DetectedLocation
        {
            get; private set;
        }
    }
}