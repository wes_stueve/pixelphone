﻿using System;
using System.Collections;

namespace PlayerDetection
{
    public class Coordinates
    {
        public float X { get; private set; }
        public float Y { get; private set; }


        public Coordinates(float x, float y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object obj)
        {
            var otherCoordinates = obj as Coordinates;
            if (otherCoordinates == null) return false;
            if (Math.Abs(otherCoordinates.X - X) < 2.0f && Math.Abs(otherCoordinates.Y - Y) < 2.0F) return true;
            return false;
        }
    }
}